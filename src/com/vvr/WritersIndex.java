package com.vvr;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.net.URL;

/**
 * Created by IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 20/9/2012
 * Time: 12:23 μμ
 * To change this template use File | Settings | File Templates.
 */
public class WritersIndex extends Activity implements View.OnClickListener {
    static final String baseURL = "http://patrologiagraeca.org/patrologia/patrologiasearch/books/index.xml";
    TextView tv;
    TextView tvWritersen;
    TextView tvCentury;
    TextView selectedVolume;
    Spinner spinnerVolume;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.writers);
        addListenerOnSpinnerItemSelection();
        Button b = (Button)findViewById(R.id.buttonSearch);
        selectedVolume = (TextView)findViewById(R.id.selectedVolume);
        tv = (TextView)findViewById(R.id.resultPanel);
        tvWritersen = (TextView)findViewById(R.id.resultPanelWritersen);
        tvCentury = (TextView)findViewById(R.id.resultPanelCentury);
        b.setOnClickListener(this);
        Button orderButton = (Button)findViewById(R.id.buttonOrder);


    }
    public void addListenerOnSpinnerItemSelection() {
        spinnerVolume = (Spinner) findViewById(R.id.spinnerVolume);
        spinnerVolume.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public void onClick(View v){
        spinnerVolume = (Spinner) findViewById(R.id.spinnerVolume);
        String volSpinner = String.valueOf(spinnerVolume.getSelectedItem());
        try{
            URL website = new URL(baseURL);
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            HandlingXMLStuff doingWork = new HandlingXMLStuff();
            doingWork.setVolume(volSpinner);
            xr.setContentHandler(doingWork);
            xr.parse(new InputSource(website.openStream()));
            String information = doingWork.getInformation();
            String informationWritersen = doingWork.getInformationWritersen();
            String informationCentury = doingWork.getInformationCentury();
            tv.setText(information);
            tvWritersen.setText(informationWritersen);
            tvCentury.setText(informationCentury);
            selectedVolume.setText(volSpinner);
            selectedVolume.setTextSize(50);
        }
        catch(Exception e){
            tv.setText("Error");
        }
    }


}
