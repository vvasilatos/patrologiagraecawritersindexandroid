package com.vvr;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 20/9/2012
 * Time: 2:14 μμ
 * To change this template use File | Settings | File Templates.
 */
public class HandlingXMLStuff extends DefaultHandler {
    boolean bwriters = false;
    boolean bnumber = false;
    boolean bwritersen = false;
    boolean bcentury = false;
    String writers = null;
    String writersen = null;
    String volume = null;
    String volumeReceived = null;
    String century = null;

    XMLDataCollected info = new XMLDataCollected();

    public String getInformation(){
        return info.dataToString();
    }

    public String getInformationWritersen(){
        return info.dataToStringWritersen();
    }

    public String getInformationCentury(){
        return info.dataToStringCentury();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("number")){
            bnumber = true;
        }
        if(qName.equals("writers")){
            bwriters = true;
        }
        if(qName.equals("writersen")){
            bwritersen = true;
        }
        if(qName.equals("century")){
            bcentury = true;
        }


    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(bnumber){
            volumeReceived = new String(ch, start, length);
            bnumber=false;

        }
        if(bwriters){
            writers = new String(ch, start, length);
            if(volumeReceived.equals(volume)){
                info.setWriters(writers);
            }
            bwriters=false;
        }
        if(bwritersen){
            writersen = new String(ch, start, length);
            if(volumeReceived.equals(volume)){
            info.setWritersen(writersen);
            }
            bwritersen=false;
        }
        if(bcentury){
             century = new String(ch, start, length);
            if(volumeReceived.equals(volume)){
                info.setCentury(century);
            }
            bcentury = false;
        }
    }

    public void setVolume(String vol){
        volume = vol;
    }
}


