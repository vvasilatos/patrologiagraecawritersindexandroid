package com.vvr;

/**
 * Created by IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 20/9/2012
 * Time: 2:14 μμ
 * To change this template use File | Settings | File Templates.
 */
public class XMLDataCollected {
    String writers = null;
    String writersen = null;
    String volume = null;
    String century = null;

    public void setVolume(String vol) {
        volume = vol;
    }

    public void setWriters(String wrt){
        writers = wrt;
    }

    public String dataToString(){
        return "Συγγραφείς: \n " + writers;
    }

    public void setWritersen(String wrten){
        writersen = wrten;
    }

    public String dataToStringWritersen(){
        return "Writers: \n  " + writersen;
    }

    public void setCentury(String cntr){
        century = cntr;
    }

    public String dataToStringCentury(){
        return "Αιώνας - Centrury: \n  " + century;
    }

}
