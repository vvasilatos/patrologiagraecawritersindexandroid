package com.vvr;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends Activity
{
    ImageButton imageButton;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        imageButton = (ImageButton) findViewById(R.id.imageButton1);

		imageButton.setOnClickListener(new View.OnClickListener() {


			public void onClick(View arg0) {

                  Intent browserIntent =
                            new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.patrologiagraeca.org/patrologia"));
			    startActivity(browserIntent);

			}

		});

        final Button writersIndex=(Button) findViewById(R.id.buttonWritersIndex);
        writersIndex.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent("patrologia graeca writers index.WRITERSINDEX"));
            }
        });

        final Button exit=(Button) findViewById(R.id.buttonExit);
        exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                finish();
                System.exit(0);
            }
        });

    }
}
