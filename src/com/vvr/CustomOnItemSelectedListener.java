package com.vvr;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

/**
 * Created by IntelliJ IDEA.
 * User: v.vasilatos
 * Date: 21/9/2012
 * Time: 12:54 μμ
 * To change this template use File | Settings | File Templates.
 */
public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        Toast.makeText(parent.getContext(),
                "You selected : " + parent.getItemAtPosition(pos).toString() + "\nClick on Search button to view the writers",
                Toast.LENGTH_SHORT).show();
    }


    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

}
